/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.valens;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskTestResultsSupport;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class LogScannerTaskConfigurator extends AbstractTaskConfigurator  implements TaskTestResultsSupport
{

    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("errormask", params.getString("errormask"));
		config.put("nonerrormask", params.getString("nonerrormask"));
        config.put("filemask", params.getString("filemask"));
        config.put("linemask", params.getString("linemask"));
        config.put("extension", params.getString("extension"));
        config.put("linesplittermask", params.getString("linesplittermask"));
        config.put("tokennumber", params.getString("tokennumber"));     
        config.put("filefound", params.getString("filefound"));   
        config.put("testsuite", params.getString("testsuite"));   
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put("errormask", "^fail:.*");
		context.put("nonerrormask", "^ok.*");
        context.put("filemask", "*.*");
        context.put("linemask", ".*");
        context.put("extension", "log");
        context.put("linesplittermask", ",");
        context.put("tokennumber", "2");     
        context.put("filefound", "true");     
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put("errormask", taskDefinition.getConfiguration().get("errormask"));
		context.put("nonerrormask", taskDefinition.getConfiguration().get("nonerrormask"));
        context.put("filemask", taskDefinition.getConfiguration().get("filemask"));
        context.put("linemask", taskDefinition.getConfiguration().get("linemask"));
        context.put("extension", taskDefinition.getConfiguration().get("extension"));
        context.put("linesplittermask", taskDefinition.getConfiguration().get("linesplittermask"));
        context.put("tokennumber", taskDefinition.getConfiguration().get("tokennumber"));  
        context.put("testsuite", taskDefinition.getConfiguration().get("testsuite"));  
        context.put("filefound", taskDefinition.getConfiguration().get("filefound"));  
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put("errormask", taskDefinition.getConfiguration().get("errormask"));
		context.put("nonerrormask", taskDefinition.getConfiguration().get("nonerrormask"));
        context.put("filemask", taskDefinition.getConfiguration().get("filemask"));
        context.put("linemask", taskDefinition.getConfiguration().get("linemask"));
        context.put("extension", taskDefinition.getConfiguration().get("extension"));
        context.put("linesplittermask", taskDefinition.getConfiguration().get("linesplittermask"));
        context.put("tokennumber", taskDefinition.getConfiguration().get("tokennumber"));
        context.put("testsuite", taskDefinition.getConfiguration().get("testsuite"));  
        context.put("filefound", taskDefinition.getConfiguration().get("filefound"));  
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);        
    }

    public boolean taskProducesTestResults(@NotNull TaskDefinition taskDefinition) {
        return true;
    }
}