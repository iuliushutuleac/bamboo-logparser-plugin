/* 
 * Copyright (C) 2016 IHutuleac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.valens;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.utils.FileVisitor;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class LogParserTask implements TaskType {

    private final TestCollationService testCollationService;

    public LogParserTask(TestCollationService testCollationService) {
        this.testCollationService = testCollationService;
    }

    public TaskResult execute(TaskContext taskContext) throws TaskException {

        final CurrentBuildResult currentBuildResult = taskContext
                .getBuildContext().getBuildResult();
        final Set<TestResults> failedTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Set<TestResults> successfulTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Set<TestResults> skippedTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final CurrentBuildResult buildResult = taskContext.getBuildContext().getBuildResult();
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        ErrorMemorisingInterceptor errorLines = new ErrorMemorisingInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);
        String suite = taskContext.getConfigurationMap().get("testsuite");
        String errormask = taskContext.getConfigurationMap().get("errormask");
        String nonerrormask = taskContext.getConfigurationMap().get("nonerrormask");
        String linemask = taskContext.getConfigurationMap().get("linemask");
        String filefound = taskContext.getConfigurationMap().get("filefound");

        String testFilePattern = taskContext.getConfigurationMap().get(
                "filemask");
        String linesplittermask = taskContext.getConfigurationMap().get("linesplittermask");
        
        Integer tokennumber = 0;
        
        if(linesplittermask != null && linesplittermask.length() == 0 )
            linesplittermask = " ";
        
        try {
            tokennumber = Integer.parseInt(taskContext.getConfigurationMap().get(
                    "tokennumber"));
        } catch (Exception e) {
            tokennumber = 0;
        }
        buildLogger.addBuildLogEntry("[" + errormask + "]");
        buildLogger.addBuildLogEntry("[" + nonerrormask + "]");
        buildLogger.addBuildLogEntry("[" + linemask + "]");
        buildLogger.addBuildLogEntry("[" + testFilePattern + "]");
        buildLogger.addBuildLogEntry("[" + linesplittermask + "]");
        buildLogger.addBuildLogEntry("[" + tokennumber + "]");

        try {
            FileSet fileSet = new FileSet();
            fileSet.setDir(taskContext.getRootDirectory());

            PatternSet.NameEntry include = fileSet.createInclude();
            include.setName(testFilePattern);

            DirectoryScanner ds = fileSet.getDirectoryScanner(new Project());
            String[] srcFiles = ds.getIncludedFiles();

            buildLogger.addBuildLogEntry("Root directory: " + taskContext.getRootDirectory());

            LogParserCollector lg = new LogParserCollector(filefound, suite, errormask, nonerrormask, linemask, linesplittermask, tokennumber);

            for (String s : srcFiles) {
                File file = new File(taskContext.getRootDirectory() + "/" + s);
                TestCollectionResult result = lg.collect(file);

                buildLogger.addBuildLogEntry(String.format("File %s parsing result: %d successful, %d failed, %d skipped",
                        file,
                        result.getSuccessfulTestResults().size(),
                        result.getFailedTestResults().size(),
                        result.getSkippedTestResults().size()));

                failedTestResults.addAll(result.getFailedTestResults());
                successfulTestResults.addAll(result.getSuccessfulTestResults());

            }

            buildResult.appendTestResults(successfulTestResults, failedTestResults, skippedTestResults);

            currentBuildResult.addBuildErrors(errorLines.getErrorStringList());

            return TaskResultBuilder.create(taskContext).checkTestFailures().build();
        } catch (Exception e) {
            e.printStackTrace();
            throw new TaskException("Failed to execute task", e);
        }
    }
}
